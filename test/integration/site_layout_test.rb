require 'test_helper'
#レイアウトのリンクに対するテスト

#ページ内にリンクを埋めているおり、それ全部が正しいかというチェックは骨が折れます
#故に統合テストを使って自動化します。
#以下のコマンドを打ってテストします
#rails test:integration

class SiteLayoutTest < ActionDispatch::IntegrationTest
  
  test "layout links" do
    get root_path
    assert_template 'static_pages/home'
    assert_select "a[href=?]", root_path, count:2     #ロゴとナビゲーションバーの2つにURLのリンクが張ってるので二回
    assert_select "a[href=?]", help_path              #about_path == ? 的に比較して探している
    assert_select "a[href=?]", about_path
    assert_select "a[href=?]", contact_path
    get contact_path
    assert_select "title",full_title("Contact")
    #assert_select "a[href=?]", signup_path
    get signup_path
    assert_select "title",full_title("Sign up")
    
  end
  
  def setup 
    @user         = users(:michael)
  end
  
  test 'ennsyuu 10 test' do
    
    log_in_as(@user)
    assert is_logged_in?
    #follow_redirect!
    get root_path
    assert_template 'static_pages/home'
    assert_select "a[href=?]", users_path
    assert_select "a[href=?]", user_path(@user)
    assert_select "a[href=?]", edit_user_path(@user)
    assert_select "a[href=?]", logout_path
    #get users_path
    #assert_select "li","Michael Hartl"#users(:michael).name

  end
  
  
end
